using Configs;

namespace Move
{
    public interface IEngineItem
    {
        float Thust { get; }
    }
}
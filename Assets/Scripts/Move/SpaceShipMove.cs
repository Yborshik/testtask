using System;
using System.Collections.Generic;
using System.Linq;
using Move;
using Systems;
using UnityEditor;
using UnityEngine;

public class SpaceShipMove : MonoBehaviour
{
    [SerializeField] private Spaceship _spaceship;
    [SerializeField] private Rigidbody _rigidbody;

    private const float _epsilon = 0.001f;
    
    private IInputSystem _inputSystem;
    private Camera _camera;

    private List<IEngineItem> _engineItems = new List<IEngineItem>();

    public void LinkDependencies(IInputSystem inputSystem)
    {
        _inputSystem = inputSystem;
    }

    private void Awake()
    {
        _camera = Camera.main;

        _engineItems = _spaceship.GetItems<IEngineItem>();
    }

    private void FixedUpdate()
    {
        Vector3 moveVector = Vector3.zero;

        if (_inputSystem.Axis.sqrMagnitude > _epsilon)
        {
            moveVector = _camera.transform.TransformDirection(_inputSystem.Axis);
            moveVector.y = 0;
            moveVector.Normalize();

            float thust = _engineItems.Sum(x => x.Thust);
            
            _rigidbody.AddForce(moveVector * thust * Time.fixedDeltaTime, ForceMode.Force);
        }
    }
}
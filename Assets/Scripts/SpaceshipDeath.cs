using UnityEngine;

public class SpaceshipDeath : MonoBehaviour
{
    [SerializeField] private Spaceship _spaceship;
    [SerializeField] private Animator _animator;
    [SerializeField] private string _deathTrigger;
    [SerializeField] private GameObject _deathEffect;
    
    private void OnEnable()
    {
        _spaceship.Health.HealthChanged += OnHealthChanged;
    }

    private void OnDisable()
    {
        _spaceship.Health.HealthChanged -= OnHealthChanged;
    }

    private void OnHealthChanged(float current, float prev)
    {
        if (current <= 0)
        {
            PlayDeath();
        }
    }

    private void PlayDeath()
    {
        _animator.SetTrigger(_deathTrigger);
        Instantiate(_deathEffect);
        _deathEffect.transform.position = transform.position;
    }

    //invoke from animation
    private void OnDeathAnimationComplete()
    {
        Destroy(_spaceship);
    }
}
using System;
using System.Collections.Generic;
using UnityEngine.Serialization;

namespace SaveData
{
    [Serializable]
    public class SpaceshipData
    {
        public string TypeId;
        public string InstanceId;
        public List<SlotData> SlotDatas = new List<SlotData>();

        public SlotData GetSlotData(string slotId)
        {
            SlotData slotData = SlotDatas.Find(x => x.Id == slotId);
            
            if(slotData == null)
            {
                slotData = new SlotData(slotId);
            }

            return slotData;
        }

        public SpaceshipData(string typeId)
        {
            TypeId = typeId;
            InstanceId = Guid.NewGuid().ToString();
        }
    }
}
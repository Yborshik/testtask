using System;

namespace SaveData
{
    [Serializable]
    public class ItemData
    {
        public string TypeId;
        public string InstanceId;

        public ItemData(string typeId)
        {
            TypeId = typeId;
            InstanceId = Guid.NewGuid().ToString();
        }
    }
}
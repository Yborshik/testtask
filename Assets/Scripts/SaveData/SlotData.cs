using System;
using System.Collections.Generic;

namespace SaveData
{
    [Serializable]
    public class SlotData
    {
        public List<string> Items = new List<string>();

        public string Id;

        public SlotData(string id)
        {
            Id = id;
        }
    }
}
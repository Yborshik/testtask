using UnityEditor;
using UnityEngine;

namespace Id
{
    [CreateAssetMenu(fileName = "DataId", menuName = "Config/Id/DataId")]
    public class DataId : ScriptableObject
    {
        [SerializeField] private string _id;

        public string GUID => _id;

        private void OnValidate()
        {
            if (!string.IsNullOrEmpty(_id))
            {
                DataId[] result = Resources.FindObjectsOfTypeAll<DataId>();

                foreach (var dataId in result)
                {
                    if (dataId != this && dataId._id == _id)
                    {
                        _id = string.Empty;
                        break;
                    }
                }
            }

            if (string.IsNullOrEmpty(_id))
            {
                string path = AssetDatabase.GetAssetPath(this);
                string guid = AssetDatabase.AssetPathToGUID(path);
                _id = guid;
            }
        }
    }
}
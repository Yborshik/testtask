using UnityEngine;

namespace Id
{
    [CreateAssetMenu(fileName = "ItemId", menuName = "Config/Id/ItemId")]
    public class ItemId : DataId
    {
        [SerializeField] private SlotTypeId _slotTypeId;
    }
}
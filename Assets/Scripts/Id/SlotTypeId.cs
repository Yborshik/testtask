using UnityEngine;

namespace Id
{
    [CreateAssetMenu(fileName = "SlotType", menuName = "Config/Id/SlotType")]
    public class SlotTypeId : DataId
    {
    }
}

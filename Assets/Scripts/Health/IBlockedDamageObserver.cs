using Id;

namespace Health
{
    public interface IBlockedDamageObserver
    {
        public event DamageBlockedDelegate DamageBlocked;

        public delegate void DamageBlockedDelegate(float blocked, DamageTypeId type);
    }
}
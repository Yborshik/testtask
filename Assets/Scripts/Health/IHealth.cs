using Id;

namespace Health
{
    public interface IHealth
    {
        float Health { get; }
        
        float MaxHealth { get; }

        bool CanTakeDamage();

        void TakeDamage(float count, DamageTypeId damageTypeId);

        void Add(float count);

        event HealthChangedDelegate HealthChanged;

        public delegate void HealthChangedDelegate(float current, float prev);
    }
}
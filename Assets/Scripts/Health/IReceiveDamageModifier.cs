using Id;

namespace Health
{
    public interface IReceiveDamageModifier
    {
        void ModifyDamage(ref float current, float total, DamageTypeId damageTypeId);
    }
}
using System.Collections.Generic;
using Id;
using UnityEngine;

namespace Health
{
    public class HealthController : MonoBehaviour, IHealth
    {
        private Spaceship _spaceship;
        
        public float Health { get; set; }
        public float MaxHealth { get; set; }

        public void Initialize(Spaceship spaceship, float maxHealth)
        {
            _spaceship = spaceship;
            MaxHealth = maxHealth;
            Health = maxHealth;
        }

        public bool CanTakeDamage()
        {
            return Health > 0;
        }

        public void TakeDamage(float damage, DamageTypeId damageTypeId)
        {
            if (Health != 0)
            {
                float currentDamage = damage;
            
                List<IReceiveDamageModifier> damageModifiers = _spaceship.GetItems<IReceiveDamageModifier>();

                foreach (IReceiveDamageModifier damageModifier in damageModifiers)
                {
                    damageModifier.ModifyDamage(ref currentDamage, damage, damageTypeId);
                }
                
                float prevHealth = Health;
                Health -= currentDamage;
                Health = Mathf.Clamp(Health, 0, MaxHealth);
                HealthChanged?.Invoke(Health, prevHealth);
            }
        }

        public void Add(float count)
        {
            float prevHealth = Health;
            Health = Mathf.Clamp(Health + count, 0, MaxHealth);
            HealthChanged?.Invoke(Health, prevHealth);
        }
        
        public event IHealth.HealthChangedDelegate HealthChanged;
    }
}
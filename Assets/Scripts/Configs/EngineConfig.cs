using System;
using System.Collections.Generic;
using UnityEngine;

namespace Configs
{
    public class EngineConfig : ItemConfig
    {
        [SerializeField] private List<EngineLevelConfig> _levelConfigs = new List<EngineLevelConfig>();

        public EngineLevelConfig GetLevelConfig(int level)
        {
            return _levelConfigs[level];
        }

        [Serializable]
        public class Stats
        {
            public float Thust;
        }
        
        [Serializable]
        public class EngineLevelConfig
        {
            public Stats Stats;
            public int UpgragePrice;
        }
    }
}
using Id;
using UnityEngine;

namespace Configs
{
    public class ItemConfig : ScriptableObject
    {
        [SerializeField] private ItemId _itemId;
        [SerializeField] private GameObject _prefab;
        [SerializeField] private float _weight;
        [SerializeField] private float _health;
        [SerializeField] private int _price;

        public ItemId Id => _itemId;
        public GameObject Prefab => _prefab;
        public float Weight => _weight;
        public float Health => _health;
        public int Price => _price;
    }
}
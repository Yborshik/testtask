using System;
using System.Collections.Generic;
using Id;
using UnityEngine;

namespace Configs
{
    public class GunConfig : ItemConfig
    {
        [SerializeField] private List<GunLevelConfig> _levelConfigs = new List<GunLevelConfig>();
        [SerializeField] private List<BulletId> _availableBullets;
        [SerializeField] private BulletId _defaultDefaultBulletId;

        public List<BulletId> AvailableBullets => _availableBullets;
        public BulletId DefaultBulletId => _defaultDefaultBulletId;

        public GunLevelConfig GetLevelConfig(int level)
        {
            return _levelConfigs[level];
        }

        [Serializable]
        public class Stats
        {
            public int Damage;
            public int Rate;
            public int AmmoCapacity;
            public float ReloadDuration;
            public float Range;
        }
        
        [Serializable]
        public class GunLevelConfig
        {
            public Stats Stats;
            public int UpgragePrice;
        }
    }
}
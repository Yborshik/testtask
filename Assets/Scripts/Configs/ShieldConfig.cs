using System;
using System.Collections.Generic;
using Id;
using UnityEngine;

namespace Configs
{
    public class ShieldConfig : ItemConfig
    {
        [SerializeField] private List<ShieldLevelConfig> _levelConfigs = new List<ShieldLevelConfig>();

        public ShieldLevelConfig GetLevelConfig(int level)
        {
            return _levelConfigs[level];
        }

        [Serializable]
        public class Stats
        {
            public float BlockPercent;
            public DamageTypeId DamageType;
        }
        
        [Serializable]
        public class ShieldLevelConfig
        {
            public Stats Stats;
            public int UpgragePrice;
        }
    }
}
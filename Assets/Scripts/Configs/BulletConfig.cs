using Id;
using UnityEngine;

namespace Configs
{
    public class BulletConfig : ScriptableObject
    {
        [SerializeField] private BulletId _bulletId;
        [SerializeField] private GameObject _bulletPrefab;

        public BulletId Id => _bulletId;
        public GameObject Prefab => _bulletPrefab;
    }
}
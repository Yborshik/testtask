using Id;
using UnityEngine;

namespace Configs
{
    public class SlotConfig : ScriptableObject
    {
        [SerializeField] private SlotTypeId _slotTypeId;
        [SerializeField] private float _maxWeight;

        public SlotTypeId Id => _slotTypeId;
        public float MaxWeight => _maxWeight;
    }
}
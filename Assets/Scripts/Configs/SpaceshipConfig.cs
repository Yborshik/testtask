using System;
using System.Collections.Generic;
using Id;
using UnityEngine;

namespace Configs
{
    public class SpaceshipConfig : ScriptableObject
    {
        [SerializeField] private SpaceshipId _spaceshipId;
        [SerializeField] private List<SpaceshipLevelConfig> _levelConfigs = new List<SpaceshipLevelConfig>();

        public SpaceshipId Id => _spaceshipId;

        public SpaceshipLevelConfig GetLevelConfig(int level)
        {
            return _levelConfigs[level];
        }

        [Serializable]
        public class Stats
        {
            public float Health;
        }
        
        [Serializable]
        public class SpaceshipLevelConfig
        {
            public Stats Stats;
            public int Price;
        }
    }
}
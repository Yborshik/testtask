using System;
using System.Collections.Generic;
using Id;
using UnityEngine;

namespace Configs
{
    public class HpGeneratorConfig : ItemConfig
    {
        [SerializeField] private List<HpGeneratorLevelConfig> _levelConfigs = new List<HpGeneratorLevelConfig>();

        public HpGeneratorLevelConfig GetLevelConfig(int level)
        {
            return _levelConfigs[level];
        }

        [Serializable]
        public class Stats
        {
            public float GeneratePercent;
            public DamageTypeId DamageType;
        }
        
        [Serializable]
        public class HpGeneratorLevelConfig
        {
            public Stats Stats;
            public int UpgragePrice;
        }
    }
}
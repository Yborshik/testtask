using System.Collections;
using UnityEngine;

namespace UI
{
    public class Tooltip : MonoBehaviour
    {
        [SerializeField]
        private float _duration = 1;

        [SerializeField]
        private float _offset;

        [SerializeField]
        private bool _fitSize;

        [SerializeField]
        private AlignType _alignType;

        [Header("Tail")]
        [SerializeField]
        private RectTransform _tailTop;

        [SerializeField]
        private RectTransform _tailBot;

        [SerializeField]
        private RectTransform _tailLeft;

        [SerializeField]
        private RectTransform _tailRight;

        [SerializeField]
        private float _tailSize;

        private RectTransform _cachedRectTransform;
        private Vector3 _position;
        private RectTransform _restrictions;


        public void Initialize(Vector3 position, RectTransform restrictions)
        {
            _position = position;
            _restrictions = restrictions;
            
            SetPosition();
            StartCoroutine(ShowDuration());
        }


        protected RectTransform CachedRectTransform
        {
            get
            {
                if (_cachedRectTransform == null)
                {
                    _cachedRectTransform = transform as RectTransform;
                }

                return _cachedRectTransform;
            }
        }

        private IEnumerator ShowDuration()
        {
            yield return new WaitForSeconds(_duration);
            gameObject.SetActive(false);
        }


        private void SetPosition()
        {
            Rect tooltipRect = CachedRectTransform.rect;
            Vector2 halfSize = new Vector2(tooltipRect.width / 2, tooltipRect.height / 2);

            Rect restrictionRect = _restrictions.rect;
            Vector3 restrictionsCenterWorldPoint = _restrictions.TransformPoint(restrictionRect.center);

            Vector2 tooltipOffset = Vector3.zero;
            Vector2 offset;
            int sign;

            if (_alignType == AlignType.Horizontal)
            {
                if (_fitSize)
                {
                    tooltipOffset =  new Vector3(halfSize.x, 0);
                }
                sign = _position.x > restrictionsCenterWorldPoint.x ? -1 : 1;

                offset = new Vector2(_offset, 0);
            }
            else
            {
                if (_fitSize)
                {
                    tooltipOffset = new Vector3(0, halfSize.y);
                }
                sign = _position.y > restrictionsCenterWorldPoint.y ? -1 : 1;

                offset = new Vector2(0, _offset);
            }

            Vector2 tooltipRestrictionLocalPosition = _restrictions.InverseTransformPoint(_position);
            tooltipRestrictionLocalPosition += offset * sign + tooltipOffset * sign;

            Vector2 clampedPosition = GetClampedPosition(tooltipRestrictionLocalPosition, restrictionRect, halfSize);

            Vector3 worldPosition = _restrictions.TransformPoint(clampedPosition);
            CachedRectTransform.position = worldPosition;

            UpdateTailPosition(sign);
        }
        
        private void UpdateTailPosition(int sign)
        {
            if (_tailTop == null || _tailBot == null || _tailLeft == null || _tailRight == null)
            {
                return;
            }
            
            RectTransform tail = GetTail(sign);
            tail.gameObject.SetActive(true);

            float halfTailSize = _tailSize / 2;
            Vector2 halfSize = new Vector2(halfTailSize, halfTailSize);

            Vector3 targetLocalPosition = CachedRectTransform.InverseTransformPoint(_position);
            Vector2 clampedPosition = GetClampedPosition(targetLocalPosition, CachedRectTransform.rect, halfSize);
            Vector2 anchoredPosition = tail.anchoredPosition;
            tail.anchoredPosition = _alignType == AlignType.Horizontal
                ? new Vector2(anchoredPosition.x, clampedPosition.y)
                : new Vector2(clampedPosition.x, anchoredPosition.y);
        }

        private RectTransform GetTail(int sign)
        {
            _tailTop.gameObject.SetActive(false);
            _tailBot.gameObject.SetActive(false);
            _tailLeft.gameObject.SetActive(false);
            _tailRight.gameObject.SetActive(false);

            if (_alignType == AlignType.Vertical)
            {
                return sign == -1 ? _tailTop : _tailBot;
            }
            else
            {
                return sign == -1 ? _tailRight : _tailLeft;
            }
        }

        private Vector2 GetClampedPosition(Vector3 localPosition, Rect restrictions, Vector2 halfSize)
        {
            float positionX = Mathf.Clamp(localPosition.x, restrictions.min.x + halfSize.x, restrictions.max.x - halfSize.x);
            float positionY = Mathf.Clamp(localPosition.y, restrictions.min.y + halfSize.y, restrictions.max.y - halfSize.y);

            return new Vector2(positionX, positionY);
        }


        public enum AlignType
        {
            Horizontal,
            Vertical
        }
    }
}
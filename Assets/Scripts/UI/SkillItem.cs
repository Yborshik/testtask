using System;
using UnityEngine;

namespace UI
{
    public class SkillItem : MonoBehaviour
    {
        public event Action<SkillItem> Clicked;

        public void OnClick()
        {
            Clicked?.Invoke(this);
        }
    }
}

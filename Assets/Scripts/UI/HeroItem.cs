using System;
using UnityEngine;

namespace UI
{
    public class HeroItem : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        [SerializeField] private string _selectParamName = "Selected";

        public event Action<HeroItem> Clicked;

        public void Select()
        {
            _animator.SetBool(_selectParamName, true);
        }

        public void UnSelect()
        {
            _animator.SetBool(_selectParamName, false);
        }

        public void OnClick()
        {
            Clicked?.Invoke(this);
        }
    }
}

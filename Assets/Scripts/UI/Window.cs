using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UI
{
    public class Window : MonoBehaviour
    {
        [SerializeField] private RectTransform _heroItemsRoot;
        [SerializeField] private Tooltip _skillInfoTooltip;
        [SerializeField] private RectTransform _restrictions;
        [SerializeField] private List<SkillItem> _skillItems;

        private List<HeroItem> _heroItems;
        private HeroItem _selectedItem;

        private void Awake()
        {
            _heroItems = _heroItemsRoot.GetComponentsInChildren<HeroItem>().ToList();
        }

        private void OnEnable()
        {
            foreach (SkillItem skillItem in _skillItems)
            {
                skillItem.Clicked += OnClickSkill;
            }

            foreach (HeroItem heroItem in _heroItems)
            {
                heroItem.Clicked += OnClickHeroItem;
            }
            
            Change(_heroItems[0]);
        }

        private void OnDisable()
        {
            foreach (SkillItem skillItem in _skillItems)
            {
                skillItem.Clicked -= OnClickSkill;
            }
            
            foreach (HeroItem heroItem in _heroItems)
            {
                heroItem.Clicked -= OnClickHeroItem;
            }
        }

        private void OnClickSkill(SkillItem skillItem)
        {
            if (_skillInfoTooltip.isActiveAndEnabled)
            {
                _skillInfoTooltip.gameObject.SetActive(false);
            }
            
            _skillInfoTooltip.gameObject.SetActive(true);
            _skillInfoTooltip.Initialize(skillItem.transform.position, _restrictions);
        }

        private void OnClickHeroItem(HeroItem heroItem)
        {
            Change(heroItem);
        }

        private void Change(HeroItem heroItem)
        {
            if (_selectedItem != heroItem)
            {
                if (_selectedItem != null)
                {
                    _selectedItem.UnSelect();
                }
                
                heroItem.Select();
                _selectedItem = heroItem;
            }
        }
    }
}

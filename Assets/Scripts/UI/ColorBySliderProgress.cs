using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [ExecuteAlways]
    public class ColorBySliderProgress : MonoBehaviour
    {
        [SerializeField] private Slider _slider;
        [SerializeField] private Image _image;
        [SerializeField] private Color _fullColor;
        [SerializeField] private Color _lowColor;

        private void Start()
        {
            OnValueChanged(_slider.value);
        }

        private void OnEnable()
        {
            _slider.onValueChanged.AddListener(OnValueChanged);
        }

        private void OnDisable()
        {
            _slider.onValueChanged.RemoveListener(OnValueChanged);
        }

        public void OnValueChanged(float value)
        {
            _image.color = Color.Lerp(_lowColor, _fullColor, value);
        }
    }
}

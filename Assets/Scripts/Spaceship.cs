using System.Collections.Generic;
using Configs;
using Factory;
using Health;
using Id;
using SaveData;
using Systems;
using UnityEngine;
using Upgrade;

public class Spaceship : MonoBehaviour
{
    [SerializeField] private SpaceshipId _spaceshipId;
    [SerializeField] private HealthController _healthController;
    [SerializeField] private List<SpaceshipSlot> _slots;

    private IItemFactory _itemFactory;
    private ItemsSystem _itemsSystem;
    private IConfigProvider _configProvider;
    private UpgradeSystem _upgradeSystem;

    private SpaceshipData _spaceshipData;
    
    private SpaceshipConfig.SpaceshipLevelConfig Config { get; set; }

    public IHealth Health => _healthController;

    public void LinkDependencies(IItemFactory itemFactory, ItemsSystem itemsSystem, IConfigProvider configProvider, UpgradeSystem upgradeSystem)
    {
        _itemFactory = itemFactory;
        _itemsSystem = itemsSystem;
        _configProvider = configProvider;
        _upgradeSystem = upgradeSystem;
    }
    
    public void Initialize(SpaceshipData spaceshipData)
    {
        _spaceshipData = spaceshipData;
        
        InitializeConfig();
        InitializeHealth();
        InitializeSlots();
    }

    public void Save()
    {
        foreach (SpaceshipSlot slot in _slots.ToArray())
        {
            SlotData slotData = _spaceshipData.GetSlotData(slot.Id);
            SpaceshipSlot spaceshipSlot = _slots.Find(x => x.Id == slotData.Id);

            if (spaceshipSlot != null)
            {
                spaceshipSlot.Save(slotData);
            }
            else
            {
                _spaceshipData.SlotDatas.Remove(slotData);
            }
        }
    }

    public bool TrySetItem(SpaceshipSlot slot, ItemData itemData)
    {
        ItemConfig itemConfig = _configProvider.GetItemConfig(itemData.TypeId);
        
        Item item = _itemFactory.CreateItem(itemData, itemConfig, this);

        if (!slot.TryAddItem(item))
        {
            Destroy(item);
            return false;
        }

        return true;
    }

    public void RemoveItem(SpaceshipSlot slot, Item item)
    {
        slot.RemoveItem(item);
        Destroy(item);
    }

    public List<T> GetItems<T>() where T : class
    {
        List<T> items = new List<T>();

        foreach (SpaceshipSlot slot in _slots)
        {
            foreach (Item item in slot.Items)
            {   
                if (item is T Titem)
                {
                    items.Add(Titem);
                }
            }
        }

        return items;
    }

    private void InitializeConfig()
    {
        SpaceshipConfig config = _configProvider.GetSpaceshipConfig(_spaceshipId);
        int level = _upgradeSystem.GetUpgradeLevel(_spaceshipData.InstanceId);
        Config = config.GetLevelConfig(level);
    }

    private void InitializeHealth()
    {
        _healthController.Initialize(this, Config.Stats.Health);
    }

    private void InitializeSlots()
    {
        foreach (SlotData slotData in _spaceshipData.SlotDatas)
        {
            SpaceshipSlot slot = _slots.Find(x => x.Id == slotData.Id);

            if (slot != null)
            {
                foreach (string itemInstanceId in slotData.Items)
                {
                    ItemData itemData = _itemsSystem.Get(itemInstanceId);

                    if (itemData != null)
                    {
                        TrySetItem(slot, itemData);
                    }
                }
            }
        }
    }
}

using System.Collections;
using Projectiles;
using UnityEngine;

namespace Items
{
    public class MachineGun : GunItem
    {
        [SerializeField] private Transform _projectileSpawnPoint;
        
        private Coroutine _fireCoroutine;

        protected override void OnStartFire()
        {
            base.OnStartFire();

            _fireCoroutine = StartCoroutine(FireCoroutine());
        }

        protected override void OnEndFire()
        {
            base.OnEndFire();
            
            StopCoroutine(_fireCoroutine);
        }

        private IEnumerator FireCoroutine()
        {
            float waitDuration = 1 / (Stats.Rate / 60f);
            
            while (AmmoCount > 0)
            {
                SpawnProjectile();
                AmmoCount -= 1;
                yield return new WaitForSeconds(waitDuration);
            }
        }

        private void SpawnProjectile()
        {
            GameObject projectileInstance = Instantiate(BulletConfig.Prefab);
            projectileInstance.transform.position = _projectileSpawnPoint.position;
            IProjectile projectile = projectileInstance.GetComponent<IProjectile>();
            projectile.Initialize(Stats.Damage, Stats.Range);
        }
    }
}
using System.Collections;
using Projectiles;
using UnityEngine;

namespace Items
{
    public class PlasmaGun : GunItem
    {
        [SerializeField] private Transform _projectileSpawnPoint;
        
        private Coroutine _fireCoroutine;
        private GameObject _projectileInstance;
        private IControlledDamageProjectile _controlledDamageProjectile;

        protected override void OnStartFire()
        {
            base.OnStartFire();

            _fireCoroutine = StartCoroutine(FireCoroutine());
        }

        protected override void OnEndFire()
        {
            base.OnEndFire();
            
            StopCoroutine(_fireCoroutine);
            OnStopFire();
        }

        private IEnumerator FireCoroutine()
        {
            float waitDuration = 1 / (Stats.Rate / 60f);
            SpawnProjectile();
            
            while (AmmoCount > 0)
            {
                _controlledDamageProjectile.DoDamage();
                AmmoCount -= 1;
                yield return new WaitForSeconds(waitDuration);
            }

            OnStopFire();
        }

        private void SpawnProjectile()
        {
            _projectileInstance = Instantiate(BulletConfig.Prefab, _projectileSpawnPoint);
            _controlledDamageProjectile = _projectileInstance.GetComponent<IControlledDamageProjectile>();
            _controlledDamageProjectile.Initialize(Stats.Damage, Stats.Range);
        }

        private void OnStopFire()
        {
            if (_projectileInstance != null)
            {
                Destroy(_projectileInstance);
                _controlledDamageProjectile = null;
            }
        }
    }
}
using Configs;
using Health;
using Id;
using SaveData;
using Upgrade;

namespace Items
{
    public class ShieldItem : Item, IReceiveDamageModifier, IBlockedDamageObserver
    {
        private UpgradeSystem _upgradeSystem;

        private ShieldConfig ShieldConfig => Config as ShieldConfig;
        public ShieldConfig.Stats Stats { get; set; }
        
        public event IBlockedDamageObserver.DamageBlockedDelegate DamageBlocked;

        public void LinkDependencies(UpgradeSystem upgradeSystem)
        {
            _upgradeSystem = upgradeSystem;
        }
        
        public override void Initialize(ItemData itemData, ItemConfig itemConfig, Spaceship spaceship)
        {
            base.Initialize(itemData, itemConfig, spaceship);
            
            int level = _upgradeSystem.GetUpgradeLevel(itemData.TypeId);
            ShieldConfig.ShieldLevelConfig levelConfig = ShieldConfig.GetLevelConfig(level);
            Stats = levelConfig.Stats;
        }
        
        public void ModifyDamage(ref float current, float total, DamageTypeId damageTypeId)
        {
            if (IsActive && Stats.DamageType == damageTypeId)
            {
                float blockedDamage = total * Stats.BlockPercent;
                current -= blockedDamage;
                
                DamageBlocked?.Invoke(blockedDamage, damageTypeId);
            }
        }
    }
}
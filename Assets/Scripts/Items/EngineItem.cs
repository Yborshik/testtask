using Configs;
using Move;
using SaveData;
using Systems;
using Upgrade;

namespace Items
{
    public class EngineItem : Item, IEngineItem
    {
        private UpgradeSystem _upgradeSystem;
        private IConfigProvider _configProvider;
        
        private EngineConfig EngineConfig => Config as EngineConfig;
        public EngineConfig.Stats Stats { get; set; }

        public float Thust
        {
            get
            {
                if (IsActive)
                {
                    return Stats.Thust;
                }
                else
                {
                    return 0;
                }
            }
        }

        public void LinkDependencies(UpgradeSystem upgradeSystem, IConfigProvider configProvider)
        {
            _upgradeSystem = upgradeSystem;
            _configProvider = configProvider;
        }
        
        public override void Initialize(ItemData itemData, ItemConfig itemConfig, Spaceship spaceship)
        {
            base.Initialize(itemData, itemConfig, spaceship);
            
            int level = _upgradeSystem.GetUpgradeLevel(itemData.TypeId);
            EngineConfig.EngineLevelConfig levelConfig = EngineConfig.GetLevelConfig(level);
            Stats = levelConfig.Stats;
        }
    }
}
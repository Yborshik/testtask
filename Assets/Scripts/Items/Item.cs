using Configs;
using Health;
using Id;
using SaveData;
using UnityEngine;

public abstract class Item : MonoBehaviour
{
    [SerializeField] private HealthController _healthController;
    
    public ItemId Id => Config.Id;
    public string InstanceId => Data.InstanceId;

    public IHealth Health => _healthController;
    public ItemConfig Config { get; private set; }
    public bool IsActive { get; private set; }
    
    protected ItemData Data { get; private set; }
    protected Spaceship Spaceship { get; private set; }
    
    public virtual void Initialize(ItemData itemData, ItemConfig itemConfig, Spaceship spaceship)
    {
        Data = itemData;
        Spaceship = spaceship;
        Config = itemConfig;

        IsActive = true;
        
        InitializeHealth();

        OnInitialized();
    }

    protected virtual void OnInitialized()
    {
    }

    protected virtual void OnEnable()
    {
        _healthController.HealthChanged += OnHealthChanged;
    }

    protected virtual void OnDisable()
    {
        _healthController.HealthChanged -= OnHealthChanged;
    }

    private void InitializeHealth()
    {
        _healthController.Initialize(Spaceship, Config.Health);
    }

    private void OnHealthChanged(float current, float prev)
    {
        if (current <= 0)
        {
            IsActive = false;
        }
    }
}

using System;
using System.Collections;
using Configs;
using Id;
using SaveData;
using Systems;
using UnityEngine;
using Upgrade;

namespace Items
{
    public abstract class GunItem : Item
    {
        private UpgradeSystem _upgradeSystem;
        private IConfigProvider _configProvider;

        private bool _fireStarted;
        private int _ammoCount;
        
        protected GunConfig GunConfig => Config as GunConfig;
        protected GunConfig.Stats Stats { get; private set; }
        protected BulletConfig BulletConfig { get; private set; }

        protected int AmmoCount
        {
            get => _ammoCount;
            set
            {
                _ammoCount += value;

                if (_ammoCount == 0)
                {
                    StartReload();
                }
            }
        }

        public event Action ReloadStart;
        public event Action ReloadEnd;
        public bool IsReloading { get; private set; }
        public float ReloadProgress { get; private set; }

        public void LinkDependencies(UpgradeSystem upgradeSystem, IConfigProvider configProvider)
        {
            _upgradeSystem = upgradeSystem;
            _configProvider = configProvider;
        }

        public override void Initialize(ItemData itemData, ItemConfig itemConfig, Spaceship spaceship)
        {
            base.Initialize(itemData, itemConfig, spaceship);

            int level = _upgradeSystem.GetUpgradeLevel(itemData.TypeId);
            GunConfig.GunLevelConfig levelConfig = GunConfig.GetLevelConfig(level);
            Stats = levelConfig.Stats;
            
            SetBulletType(GunConfig.DefaultBulletId);
        }

        protected override void OnInitialized()
        {
            base.OnInitialized();

            AmmoCount = Stats.AmmoCapacity;
        }

        public bool IsBulletSuit(BulletId bulletId)
        {
            return GunConfig.AvailableBullets.Contains(bulletId);
        }

        public void SetBulletType(BulletId bulletId)
        {
            if (IsBulletSuit(bulletId))
            {
                BulletConfig = _configProvider.GetBulletConfig(bulletId);
            }
        }

        public void StartFire()
        {
            if (IsActive && !IsReloading)
            {
                _fireStarted = true;
                OnStartFire();
            }
        }

        public void EndFire()
        {
            if (_fireStarted)
            {
                _fireStarted = false;
                OnEndFire();
            }
        }

        protected virtual void OnStartFire()
        {
        }

        protected virtual void OnEndFire()
        {
        }

        private void StartReload()
        {
            IsReloading = true;
            StartCoroutine(ReloadCoroutine());
            ReloadStart?.Invoke();
        }

        private IEnumerator ReloadCoroutine()
        {
            ReloadProgress = 0;

            float timer = 0;
            float duration = Stats.ReloadDuration;

            while (timer < duration)
            {
                timer += Time.deltaTime;
                ReloadProgress = timer / duration;
                yield return null;
            }

            OnReloadEnd();
        }

        private void OnReloadEnd()
        {
            ReloadProgress = 1;
            IsReloading = false;
            AmmoCount = Stats.AmmoCapacity;
            ReloadEnd?.Invoke();
        }
    }
}
using System.Collections.Generic;
using Configs;
using Health;
using Id;
using SaveData;
using Systems;
using Upgrade;

namespace Items
{
    public class HpGeneratorItem : Item
    {
        private UpgradeSystem _upgradeSystem;

        private List<IBlockedDamageObserver> _blockedDamageObservers;
        
        private HpGeneratorConfig HpGeneratorConfig => Config as HpGeneratorConfig;
        public HpGeneratorConfig.Stats Stats { get; set; }

        public void LinkDependencies(UpgradeSystem upgradeSystem)
        {
            _upgradeSystem = upgradeSystem;
        }
        
        public override void Initialize(ItemData itemData, ItemConfig itemConfig, Spaceship spaceship)
        {
            base.Initialize(itemData, itemConfig, spaceship);
            
            int level = _upgradeSystem.GetUpgradeLevel(itemData.TypeId);
            HpGeneratorConfig.HpGeneratorLevelConfig levelConfig = HpGeneratorConfig.GetLevelConfig(level);
            Stats = levelConfig.Stats;
        }
        
        protected override void OnInitialized()
        {
            base.OnInitialized();

             _blockedDamageObservers = Spaceship.GetItems<IBlockedDamageObserver>();
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            foreach (IBlockedDamageObserver damageObserver in _blockedDamageObservers)
            {
                damageObserver.DamageBlocked += OnDamageBlocked;
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            
            foreach (IBlockedDamageObserver damageObserver in _blockedDamageObservers)
            {
                damageObserver.DamageBlocked -= OnDamageBlocked;
            }
        }
        
        private void OnDamageBlocked(float blocked, DamageTypeId type)
        {
            if (IsActive && Stats.DamageType == type)
            {
                float regenerateHealth = blocked * Stats.GeneratePercent;
                Spaceship.Health.Add(regenerateHealth);
            }
        }
    }
}
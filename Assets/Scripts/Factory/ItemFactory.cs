using Configs;
using SaveData;
using UnityEngine;

namespace Factory
{
    public class ItemFactory : IItemFactory
    {
        public Item CreateItem(ItemData itemData, ItemConfig itemConfig, Spaceship spaceship)
        {
            GameObject instance = Object.Instantiate(itemConfig.Prefab);
            Item item = instance.GetComponent<Item>();
            item.Initialize(itemData, itemConfig, spaceship);
            return item;
        }
    }
}
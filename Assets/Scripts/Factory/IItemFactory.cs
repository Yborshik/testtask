using Configs;
using SaveData;

namespace Factory
{
    public interface IItemFactory
    {
        Item CreateItem(ItemData itemData, ItemConfig itemConfig, Spaceship spaceship);
    }
}
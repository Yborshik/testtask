using Health;
using Id;
using UnityEngine;

namespace Projectiles
{
    public class PlasmaProjectile : MonoBehaviour, IControlledDamageProjectile
    {
        [SerializeField] private float _damageMultiplier = 1;
        [SerializeField] private float _distanceMultiplier = 1;
        [SerializeField] private DamageTypeId _damageTypeId;
        [SerializeField] private LineRenderer _lineRenderer;

        private float _damage;
        private float _maxDistance;
        private Vector3[] _positions = new Vector3[2];
        private RaycastHit _raycastHit;
        private bool _hasHit;

        public void Initialize(float damage, float maxDistance)
        {
            _damage = damage * _damageMultiplier;
            _maxDistance = maxDistance * _distanceMultiplier;
        }
        
        public void DoDamage()
        {
            if (_hasHit)
            {
                IHealth health = _raycastHit.transform.GetComponent<IHealth>();

                if (health != null && health.CanTakeDamage())
                {
                    health.TakeDamage(_damage, _damageTypeId);
                }
            }
        }

        private void Update()
        {
            _positions[0] = transform.position;

            if(Physics.Raycast(transform.position, transform.forward, out _raycastHit, _maxDistance))
            {
                _hasHit = true;
                _positions[1] = _raycastHit.point;
            }
            else
            {
                _hasHit = false;
                _positions[1] = transform.forward * _maxDistance;
            }
            
            _lineRenderer.SetPositions(_positions);
        }
    }
}
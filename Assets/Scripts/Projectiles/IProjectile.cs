namespace Projectiles
{
    public interface IProjectile
    {
        void Initialize(float damage, float distance);
    }
}
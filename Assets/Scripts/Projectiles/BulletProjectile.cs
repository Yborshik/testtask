using Health;
using Id;
using UnityEngine;

namespace Projectiles
{
    public class BulletProjectile : MonoBehaviour, IProjectile
    {
        [SerializeField] private float _damageMultiplier = 1;
        [SerializeField] private float _distanceMultiplier = 1;
        [SerializeField] private float _speed;
        [SerializeField] private DamageTypeId _damageTypeId;

        private float _damage;
        private float _maxDistance;
        private Vector3 _startPosition;
        
        public void Initialize(float damage, float maxDistance)
        {
            _damage = damage * _damageMultiplier;
            _maxDistance = maxDistance * _distanceMultiplier;
            _startPosition = transform.position;
        }

        private void OnCollisionEnter(Collision collision)
        {
            IHealth health = collision.transform.GetComponent<IHealth>();
            if (health != null && health.CanTakeDamage())
            {
                health.TakeDamage(_damage, _damageTypeId);
                Destroy(gameObject);
            }
        }

        private void FixedUpdate()
        {
            transform.Translate(transform.forward * Time.fixedDeltaTime * _speed);
            if (Vector3.Distance(_startPosition, transform.position) > _maxDistance)
            {
                Destroy(gameObject);
            }
        }
    }
}
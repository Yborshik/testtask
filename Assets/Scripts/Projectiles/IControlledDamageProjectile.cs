namespace Projectiles
{
    public interface IControlledDamageProjectile : IProjectile
    {
        void DoDamage();
    }
}
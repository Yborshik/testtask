using System.Collections.Generic;
using Configs;
using Id;
using SaveData;
using Systems;
using UnityEngine;

public class SpaceshipSlot : MonoBehaviour
{
    [SerializeField] private SlotTypeId _slotType;
    [SerializeField] private List<Transform> _slotPositions;
    [SerializeField] private string _slotId;

    private Dictionary<Item, Transform> _lockedPositions = new Dictionary<Item, Transform>();
    private List<Transform> _availablePositions = new List<Transform>();

    private SlotConfig Config { get; set; }

    public List<Item> Items { get; } = new List<Item>();
    
    public string Id => _slotId;

    public void LinkDependencies(IConfigProvider configProvider)
    {
        Config = configProvider.GetSlotConfig(_slotType);
        _availablePositions = _slotPositions;
    }

    public bool TryAddItem(Item item)
    {
        float possibleWeight = CalculateWeight() + item.Config.Weight;
        
        if (_availablePositions.Count > 0 &&
            possibleWeight < Config.MaxWeight)
        {
            Transform position = PopFreePosition();
            _lockedPositions.Add(item, position);
            item.transform.SetParent(position);

            return true;
        }
        else
        {
            return false;
        }
    }

    public void RemoveItem(Item item)
    {
        Items.Remove(item);
        AddFreePosition(item);
    }

    public void Save(SlotData data)
    {
        data.Items = new List<string>();

        foreach (Item item in Items)
        {
            data.Items.Add(item.InstanceId);
        }
    }

    private float CalculateWeight()
    {
        float totalWeight = 0;

        foreach (Item item in Items)
        {
            totalWeight += item.Config.Weight;
        }

        return totalWeight;
    }

    private Transform PopFreePosition()
    {
        int peekIndex = _availablePositions.Count - 1;
        Transform position = _availablePositions[peekIndex];
        _availablePositions.RemoveAt(peekIndex);
        return position;
    }

    private void AddFreePosition(Item item)
    {
        Transform position = _lockedPositions[item];
        _lockedPositions.Remove(item);
        _availablePositions.Add(position);
    }
}

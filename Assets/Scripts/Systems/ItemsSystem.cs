using System.Collections.Generic;
using SaveData;

namespace Systems
{
    public class ItemsSystem
    {
        private List<ItemData> _items = new List<ItemData>();

        public void Add(ItemData itemData)
        {
            _items.Add(itemData);
        }

        public ItemData Get(string instanceId)
        {
            return _items.Find(x => x.InstanceId == instanceId);
        }
    }
}
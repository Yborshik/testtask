using UnityEngine;

namespace Systems
{
    public interface IInputSystem
    {
        Vector2 Axis { get; }
    }
}
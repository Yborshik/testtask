using System;
using System.Collections.Generic;

namespace Upgrade
{
    public class UpgradeSystem
    {
        private Dictionary<string, int> _upgradeMap = new Dictionary<string, int>();

        public void SetUpgrade(string id, int level)
        {
            if (_upgradeMap.ContainsKey(id))
            {
                _upgradeMap[id] = level;
            }
            else
            {
                _upgradeMap.Add(id, level);
            }
        }
        
        public int GetUpgradeLevel(string id)
        {
            _upgradeMap.TryGetValue(id, out int level);
            return level;
        }


        public void LoadFromSave(List<UpgradeData> upgrades)
        {
            foreach (UpgradeData upgradeData in upgrades)
            {
                _upgradeMap.Add(upgradeData.InstanceId, upgradeData.Level);
            }
        }

        public List<UpgradeData> Save()
        {
            List<UpgradeData> upgradeDatas = new List<UpgradeData>();

            foreach (KeyValuePair<string,int> pair in _upgradeMap)
            {
                upgradeDatas.Add(new UpgradeData() {InstanceId = pair.Key, Level = pair.Value});
            }

            return upgradeDatas;
        }
        
        [Serializable]
        public class UpgradeData
        {
            public string InstanceId;
            public int Level;
        }
    }
}
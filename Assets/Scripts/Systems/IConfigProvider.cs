using Configs;
using Id;

namespace Systems
{
    public interface IConfigProvider
    {
        public ItemConfig GetItemConfig(string itemId);
        public BulletConfig GetBulletConfig(BulletId bulletId);
        public SlotConfig GetSlotConfig(SlotTypeId slotTypeId);
        public SpaceshipConfig GetSpaceshipConfig(SpaceshipId spaceshipId);
    }
}